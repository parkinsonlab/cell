/*
===============================================================================

	FILE:  mechcontrols.h
	
	PROJECT:
	
		GLOW widgets for glutmech demo
	
	CONTENTS:
	
		Controls window
	
	PROGRAMMERS:
	
		Daniel Azuma <dazuma@kagi.com>
	
	COPYRIGHT:
	
		Copyright (C) 2000  Daniel Azuma  (dazuma@kagi.com)
		
		This library is free software; you can redistribute it and/or
		modify it under the terms of the GNU Lesser General Public
		License as published by the Free Software Foundation; either
		version 2.1 of the License, or (at your option) any later version.
		
		This library is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
		GNU Lesser General Public License for more details.
		
		You should have received a copy of the GNU Lesser General Public
		License along with this library; if not, write to
			Free Software Foundation, Inc.
			59 Temple Place, Suite 330
			Boston, MA 02111-1307 USA
	
	VERSION:
	
		The GLOW Toolkit tutorial -- version 1.0.3  (10 September 2000)
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef SIMCONTROLS__H
#define SIMCONTROLS__H


/*
===============================================================================
	Headers
===============================================================================
*/

#include "glow.h"
#include "glowQuickPalette.h"

GLOW_NAMESPACE_USING

#include "BallWidget.h"


/*
===============================================================================
	Controls class
===============================================================================
*/

// SimControls is an object that receives a number of events.
// The idle event receiver is used for background calculating.
// The TextFieldWindow event receiver is used for the save image dialog.
// The other event receivers are widgets in the control palette.

class SimControls :
	public GlowPushButtonReceiver,
	public GlowSliderReceiver,
	public GlowCheckBoxReceiver,
	public BallReceiver
{
	public:
	
		SimControls(
			int mainWindowID);
		virtual ~SimControls();
	
	protected:
	
		// Widget events
		
		// Implement this method to receive pushbutton events
		virtual void OnMessage(
			const GlowPushButtonMessage& message);
		
		// Implement this method to receive slider events
		virtual void OnMessage(
			const GlowSliderMessage& message);
		
		// Implement this method to receive check box events
		virtual void OnMessage(
			const GlowCheckBoxMessage& message);
		
		// Implement this method to receive ball events (new with lesson 8)
		virtual void OnMessage(
			const BallMessage& message);
	
	private:
	
		// Main drawing window id
		int _mainWindowID;
		
		// Pointer to the control window itself
		GlowQuickPaletteWindow* _controlWindow;
		
		// Pointers to selected widgets. We store these so that we can tell
		// what widget was hit when we receive events.
		GlowPushButtonWidget* _quitButton;
		BallWidget* _viewBall;
		BallWidget* _lightBall;
};


#endif
