/*
===============================================================================

	FILE:  Matrix.h
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Matrix 
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef MATRIX_H
#define MATRIX_H

#include "Tuple.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

using namespace std;

class Matrix{

	public:
		Matrix(Tuple *);
		Matrix(int, int);
		~Matrix();
		
		void add(Matrix *);
		void addAndClear(Matrix *);
		void scalarize(float);
		void addValueToRow(int, Tuple *);
		void copyValueToRow(int, Tuple *);
		void appendRow(Tuple *);
		float get(int, int);
		Tuple * collapse();

		Tuple *  getRow(int);

		int getNumRows();
		int getNumCols();
		void display();

		vector <Tuple *> matrix;

	private:
		int numRows;
		int numCols;
};

#endif

