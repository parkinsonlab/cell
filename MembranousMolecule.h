/*
===============================================================================

	FILE:  MembranousMolecule.h
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Membranous Molecule
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef MembranousMolecule_H
#define MembranousMolecule_H

#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2

#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>
#include "Molecule.h"
#include "RandomNG.h"
#include "definition.h"
#include "functionLib.h"

class MembranousMolecule: public Molecule{

	public:
	
		//////////////////////////////////////////////////////////////////////
		// Constructor and Destructor
		MembranousMolecule();
		MembranousMolecule(Point &);
		virtual ~MembranousMolecule();
		moltype type;   // typedef int moltype
		int state;
		int openedAt;

		//////////////////////////////////////////////////////////////////////
		// Operations on the MembranousMolecule		

		
		//////////////////////////////////////////////////////////////////////
		// Virtual Methods
		
		// Get the group name of the molecule
		virtual string getGroupName();
		
		// Get the Group of the molecule
		virtual int getGroup();

		moltype getType();  // typedef int moltype
		string getTypeName();
		
		void move();

	protected:

		//////////////////////////////////////////////////////////////////////
		// Member variables
		int xMin; int xMax;
		int yMin; int yMax;
		int zMin; int zMax;
		
		// The axis that is fixed (the surface of membrane)
		int fixedAxis;
			
		// The Parameter Manager
		ParameterManager *pm;
		
		Vector3D getTranslationVector();		
		
}
;

#endif
