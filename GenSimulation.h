#ifndef GENSimulation_H
#define GENSimulation_H

#include "Simulation.h"

#include "MembranousMolecule.h"
#include "Enzyme.h"

using namespace std;

class GenSimulation : public Simulation
{
	public:

		static GenSimulation * getSimulation();

		bool isSimulationCompleted();

	protected:

		GenSimulation();
		virtual ~GenSimulation();

	private:

		//////////////////////////////////////////////////////////////////////
		// Member variables

		//////////////////////////////////////////////////////////////////////
		// Private Operations
		
		// Input Data
		void initialDataInput();

		// Create new molecule of specified type
		Molecule * createNewMolecule(int);

		// Periodically update Behaviour/Properties of the Cell TNF_Simulation
		void updateSimulation();

		// Display data from simulation
		void displayResults();
}
;

#endif
