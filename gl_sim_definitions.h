 #ifndef GL_SIM_DEFINITIONS_H
#define GL_SIM_DEFINITIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <map>
#include <vector>

#include "Molecule.h"
#include "Matrix.h"
#include "GenSimulation.h"
#include "functionLib.h"
#include "ParameterManager.h"
#include "LatticeEnvironment.h"

// Manages dynamic parameters
Simulation * cell;
ParameterManager * pm = ParameterManager::getParameterManager();

// Dimensions of the Lattice
int xDim = pm->get("X_DIM");
int yDim = pm->get("Y_DIM");
int zDim = pm->get("Z_DIM");

// Dimensions of the Nucleus
int nuc_xDim = pm->get("NUC_X_DIM");
int nuc_yDim = pm->get("NUC_Y_DIM");
int nuc_zDim = pm->get("NUC_Z_DIM");

int numLatticeSites = xDim * yDim * zDim;
int numEnvironments= 5;
float resolution = 0.25;
LatticeEnvironment * lEnv = LatticeEnvironment::getLatticeEnv();



///////////////////////////////////////////////////////////////////////
// OPEN GL declarations
///////////////////////////////////////////////////////////////////////

// Include OpenGL libraries
#include <GL/gl.h>
#include <GL/glut.h>
#include <plib/ul.h>
#include <plib/sg.h>
#include <plib/fnt.h>
#include <plib/pu.h>
#include <plib/ssg.h>
#include <plib/sl.h>
#include <X11/X.h>
#include "Viewer.h"

#define KEY_ESC 27
#define KEY_LOOK_UP 112
#define KEY_LOOK_DOWN 80
#define KEY_LOOK_LEFT 116
#define KEY_LOOK_RIGHT 84
#define KEY_SLIDE_LEFT 104
#define KEY_SLIDE_UP 118
#define KEY_ROLL_LEFT 82
#define KEY_SLIDE_RIGHT 72
#define KEY_SLIDE_DOWN 86
#define KEY_ROLL_RIGHT 114
#define KEY_N 110
#define KEY_L 108
#define KEY_AXIS 97
#define KEY_PAUSE 32
#define KEY_ZOOM_IN 62
#define KEY_ZOOM_OUT 60
#define KEY_ROTATE_X 120
#define KEY_REV_ROTATE_X 88
#define KEY_ROTATE_Y 121
#define KEY_REV_ROTATE_Y 89
#define KEY_ROTATE_Z 122
#define KEY_REV_ROTATE_Z 90
#define DELTA 5
#define KEY_DISPLAY_SIMULATION 13

#define KEY_META_A_ON 49 // '1'
#define KEY_META_A_OFF 33 // !''
#define KEY_META_B_ON 50 // '2'
#define KEY_META_B_OFF 64 // @''
#define KEY_META_C_ON 51 // '3'
#define KEY_META_C_OFF 35 // '#'
#define KEY_META_D_ON 52 // '4'
#define KEY_META_D_OFF 36 // '$'
#define KEY_META_E_ON 53 // '5'
#define KEY_META_E_OFF 37 // '%'
#define KEY_META_ALL_ON 96 // '`'
#define KEY_META_ALL_OFF 126 // '~'

extern void setDrawingColor(int, bool);
extern void setDisplayEnvironment();
extern void keyboard(unsigned char, int, int);
extern void constructLattice();
extern void init();


float xOrigin = 0;
float yOrigin = 0;
float zOrigin = 0;

// Rotations
int xAngle = 0;
int yAngle = 0;
int zAngle = 0;
int rotationSpeed = 5;
int concType = (-1);
int showEnvType = (-1);

float zoomValue = 1;
float zoomSpeed = 0.1;
float horizontal = 0;
float horizontalSpeed = 0.2;
float vertical = 0;
float verticalSpeed = 0.2;


// Shade the Nucleus and the Cell
bool shadeNucleus = true;
bool shadeCell = true;
bool showAxis = false;
bool pauseSimulation = false;
bool view_perspective = true;
bool displaySimulation = true;

bool ended = false;


//Colors and Materials
GLfloat COL_MOL_ENZYME_A[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat COL_MOL_ENZYME_B[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat COL_MOL_ENZYME_C[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat COL_MOL_ENZYME_D[] = { 0.0f, 0.8f, 0.8f, 1.0f };
GLfloat COL_MOL_ENZYME_E[] = { 0.8f, 0.0f, 0.8f, 1.0f };

GLfloat ACT[] = {1.0f, 1.0f, 1.0f, 0.0f};
GLfloat AXIS[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LATTICE[] = {0.0f, 0.0f , 0.0f, 0.01f };
GLfloat NUCLEUS[] = {0.5f, 0.5f , 0.0f, 0.01f};

///////////////////////////////////////////////////////////////////////

#endif
