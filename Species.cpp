/*
===============================================================================

	FILE:  Species.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Parameter Manager
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "Species.h"
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

modifier = new string[numSpecies];

Species::Species(Species_t* s) {
	orig = s;
}

Species::Species(Species_t* s, string t) {
	orig = s;
	type = t;
}

Species::~Species() {
	printf("Deleting Species\n");
}

Species_t* Species::get() {
	return orig;
}

string Species::getType() {
	if(areSame(type, "") == 0) {
		determineType();
	}
	return type;
}

string Species::getProduct() {
	return product;
}

int Species::getNumModifiers {
	return numModifiers;
}

string Species::getModifier(int) {
	return modifier[int];
}

string Species::getPrev() {
	return prev;
}

string Species::getNext() {
	return next;
}

void Species::setReactant(string r) {
	reactant = t;
}

void Species::setProduct(string p) {
	product = p;
}

void Species::setModifier(string m) {
	modifier[numModifiers] = m;
	numModifiers++;
}

void Species::setPrev(string p) {
	prev = p;
}

void Species::setNext(string n) {
	next = n;
}

void Species::determineType() {
	if(!areSame(prev, "")) {
		if(!areSame(next, "")) {
			if(numModifiers > 0) {
				if(areSame(prev, next)) /* switches between active and inactive states */ {
					type = "Inactive"; // Inactive
				} else {
					type = "Middle";
				}
			} else {
				if(areSame(prev, next)) /* switches between active and inactive states */ {
					type = "Active"; // Activated 
				}
			}
		} else {
			
		}
	} else {
		if(!areSame(next, "")) {
			if(numModifiers > 0) {
				
			}
		}
	}
}

bool Species::areSame(string a, string b) {
	if (strcmp(a.c_str(), b.c_str()) == 0) {
		return true;
	} 
	return false;
}
	