#ifndef Enzyme_H
#define Enzyme_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>
#include "Molecule.h"
#include "RandomNG.h"
#include "definition.h"
#include "functionLib.h"
#include "IntracellularMolecule.h"

class Enzyme: public IntracellularMolecule{

	public:
	
		//////////////////////////////////////////////////////////////////////
		// Constructor and Destructor
		Enzyme(moltype);
		Enzyme(moltype, Point &);
		virtual ~Enzyme();

		//////////////////////////////////////////////////////////////////////
		// Operations on the Enzyme		
		
		//////////////////////////////////////////////////////////////////////
		// Virtual Methods
		
		// Get the group name of the molecule
		virtual string getGroupName();
		
		// Get the Group of the molecule
		virtual int getGroup();
		
		moltype getType();
		
		string getTypeName();
		
		moltype type;
		int reactant;
		int product;
		float reactionRate;
		float forwardReactionRate;
		float reverseReactionRate;
		float equilibriumConstant;
		float maximumRate;

		void setReactant(int);
		void setProduct(int);
		void setParameters(float, float, float, float);
		void setParameters();

		//virtual void activate(Molecule *);

	protected:

        // Compare Enzyme Names

        //int compareEnzymeName(int numEnzyme, multimap<int, string>*molEnzyme);
		//////////////////////////////////////////////////////////////////////
		// Member variables
			
		// The Parameter Manager
		ParameterManager *pm;
		
		void init();
			
}
;

#endif
