#ifndef GLOBAL_H
#define GLOBAL_H

#include "common/extern.h"
#include <iostream>
#include "SBase.h"
#include "common/sbmlfwd.h"
#include "sbml/SBMLTypes.h"


SBMLDocument_t *d;
Model_t *m;
Species_t **s;
Species_t **e;
Species_t **subs;
string classInfo;
int numEnzyme;
int numSubstrate;
int *molEnzyme;
int *molSubstrate;
int *numMolEnzyme;
int *numMolSubstrate;

