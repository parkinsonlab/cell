/*
===============================================================================

	FILE:  Point.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Point
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "Point.h"

Point :: Point(){}

Point :: Point(float _x, float _y, float _z){
	x = _x; y = _y, z = _z;
}

Point :: Point(int _x, int _y, int _z){
	x = _x; y = _y, z = _z;
}

Point :: Point(const Point & p){

	x = p.x;
	y = p.y;
	z = p.z;
}

Point :: ~Point(){}

void Point :: add(Point & p){
	x += p.x;
	y += p.y;
	z += p.z;
}

void Point :: set(const Point & p){
	x = p.x;
	y = p.y;
	z = p.z;
}

float Point :: distFromOrigin(){
	return sqrt(pow(x,2) + pow(y,2) + pow(z,2));
}

