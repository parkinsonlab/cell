/*
===============================================================================

	FILE:  ParameterManager.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Parameter Manager
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "ParameterManager.h"
#include "definition.h"
#include "IntracellularMolecule.h"
#include "MembranousMolecule.h"

extern SBMLDocument_t *d;
extern Model_t *m;
extern Species_t **s;
extern map<int, Species_t *> e;
extern map<int, Species_t *> subs;
extern map<int, Species_t *> prot;
extern map<int, Species_t *> mets;
extern string classInfo;
extern int numSpecies; 
extern int numEnzyme;
extern int numSubstrate;
extern int numProtein;
extern int numMetabolite;
extern multimap<int, string> molEnzyme;
extern multimap<int, string> molSubstrate;
//extern int *numMolEnzyme;
//extern int *numMolSubstrate;
extern int *numMolecule;
extern Reaction_t **rxns;
extern map<int, Species_t *> reactants;
extern map<int, Species_t *> products;
extern map<int, KineticLaw_t *> klaw;
extern map<int, Species_t *> initialMet;
extern map<int, Species_t *> finalMet;
extern map<int, Species_t *> finalSigEnzyme;
extern map<string, int> protType;
extern map<int, int> metIndex;
  

// State of the Parameter Manager
bool ParameterManager::isInstantiated = false;

// Pointer to the Parameter Manager
ParameterManager * ParameterManager::params;

//////////////////////////////////////////////////////////////////////
// Get an instance of the Parameter Manager
//
ParameterManager * ParameterManager::getParameterManager(){

	// Create new Parameter Manager if not yet exist
	if(!isInstantiated){
	
		// Create Parameter

		ParameterManager * new_param = new ParameterManager();
		
		// Assign Pointer to existing Parameter Manager
		params = new_param;
		
		// Parameter Manager created
		isInstantiated = true;
	}

	// Return pointer to the current Parameter Manager
	return params;
}

//////////////////////////////////////////////////////////////////////
// Get value of the Parameter by the specified KEY
//
int ParameterManager::get(string key){

	// Iterate through the parameter table
	map<string, int >::const_iterator itr = parameterTable.find(key);
			
	// If the KEY is found in the parameter table
	if(itr != parameterTable.end()){
		
		return (itr->second);
	}
	
	// Return NIL if KEY not found in the parameter table
	return 0;
}



//////////////////////////////////////////////////////////////////////
// Add parameter (key, value) pair
//
void ParameterManager::add(string key, int value){
	parameterTable[key] = value;
}



//////////////////////////////////////////////////////////////////////
// Constructor for the Parameter Manager
// Population the parameter table
//
ParameterManager::ParameterManager(){

	// Load default parameter file
	load(INPUT_FILE_PARAMS);
}

// BY SUKWON
void ParameterManager::loadSBML(const char* fName){
    char *checkMoleculeType = NULL;
    
    ofstream inputFile("inputParam.txt", ios::out);

    inputFile << "X_DIM=10" << endl
        << "Y_DIM=10" << endl
        << "Z_DIM=10" << endl;
    
    d = readSBML(fName);
    if (d->getNumFatals() > 0) { 
    	cout << "File not found!" << endl; 
    }
    
    m = SBMLDocument_getModel(d);
    //cout << SBase_getAnnotation(m);

    // modified by Sukwon below
    numSpecies = Model_getNumSpecies(m);
    cout << endl << numSpecies << endl;
    s = new Species_t* [numSpecies];

    /*e = new Species_t* [numSpecies];
    subs = new Species_t* [numSpecies];
    prot = new Species_t* [numSpecies];
    mets = new Species_t* [numSpecies];*/
    //reactants = new int [numSpecies];
    //products = new int [numSpecies];
    //klaw = new KineticLaw_t* [numSpecies];
    /*numMolEnzyme = new int [numSpecies]; 
    numMolSubstrate = new int [numSpecies];*/
    numMolecule = new int [numSpecies];
    //initialMet = -1;
    //finalMet = -1;
            
    for (int i = 0; i < numSpecies; i++) {
    	s[i] = Model_getSpecies(m, i);
    	numMolecule[i] = Species_getInitialAmount(s[i]);
    }
    
    int numRxns = Model_getNumReactions(m);
    rxns = new Reaction_t* [numRxns];

    /*for(int i=0; i<numSpecies; i++) {
        reactants[i] = -1;
        products[i] = -1;
        e[numSpecies] = NULL;
    }*/
    
    
    /*rxns = new Reaction_t* [numRxns];
    for(int i=0; i<numRxns; i++)
    {
        rxns[i] = Model_getReaction(m, i);
        int numReactants = Reaction_getNumReactants(rxns[i]);
        int numProducts = Reaction_getNumProducts(rxns[i]);
        int numMods = Reaction_getNumModifiers(rxns[i]);
        const char* tempId;
        for (int j=0; j<numReactants; j++) {
        	tempId = SimpleSpeciesReference_getSpecies(Reaction_getReactant(rxns[i], j));
        	Model_getSpeciesById(m, tempId);
        }
    }*/
    
    for(int i=0; i<numRxns; i++) {
        rxns[i] = Model_getReaction(m, i);
    }
    
    string annot = SBase_getAnnotation(m);
    size_t pos = annot.find("listOfProteins") + 14;
    string protNames = annot.substr(pos); 
    size_t last_pos = protNames.find("listOfProteins");
    string mProtNames = protNames.substr(0, last_pos);
   	pos = mProtNames.find("name=") + 6;
    while(pos != 5) /*find(<string>) does not return -1*/ {
    	size_t mid_pos = mProtNames.find("type", pos) - 2;
    	string name = mProtNames.substr(pos, mid_pos - pos);
    	//cout << "Name read: " << name << endl;
    	protType[name] = numProtein;
    	numProtein++;
    	pos = mProtNames.find("name=", mid_pos) + 6;
    }
    
    Species_t* tempSp; 
    
//    string empt = "";
    for(int i=0; i<numSpecies; i++) {
        classInfo = SBase_getAnnotation(s[i]);
        checkMoleculeType = strstr (classInfo.c_str(), "PROTEIN");
		bool modifies = false;
        
        cout << "Species #" << i << " is " << Species_getName(s[i]) << endl;
		
        if(checkMoleculeType != NULL) {
        	prot[i] = s[i]; 
        	//numProtein++;
        	for(int j=0; j < numRxns; j++) {
        		//cout << "Reaction #" << j << " out of " << numRxns;
            	for(int mods=0; mods<Reaction_getNumModifiers(rxns[j]); mods++) {
            		//cout << " Checking modifiers.";
		        	if(strcmp(SimpleSpeciesReference_getSpecies(Reaction_getModifier(rxns[j], mods)), Species_getId(s[i])) == 0) {
		        		//cout << " It's a modifier! "; //cout << (e[numEnzyme] == NULL) << " ~~ "; 		        		
		        		modifies = true;
		        		//if(e[numEnzyme] == NULL) {
		        		//	cout << "Is it ever null?" << endl;
			        	e[i] = s[i];
			        	//numMolecule[i] = Species_getInitialAmount(s[i]);
    	        		//numMolEnzyme[numEnzyme] = Species_getInitialAmount(s[i]);
		        		//} else { 
		        		//	cout << typeid(e[numEnzyme]).name();
		        		//	cout << " " << typeid(Species_getName(e[numEnzyme])).name() << endl;
		        		//	if(strcmp(Species_getId(e[numEnzyme]), empt.c_str()) == 0) {
		        		//	cout << "Is it ever empty?" << endl;
			        	//	e[numEnzyme] = Model_getSpecies(m, i);
    	        		//	numMolEnzyme[numEnzyme] = Species_getInitialAmount(s[i]);
		        		//	}
		        		//}
		        	   	
		        	   	int enzType = -1;
		        	   	map<string, int>::const_iterator tItr = protType.find(Species_getName(s[i]));
		        	   	if(tItr != protType.end()) {
		        	   		enzType = tItr->second; 
		        	   	}
		        	   	assert(enzType>=0);
		        	   	
		        	   	//cout << Species_getName(s[i]) << " / " << Species_getName(e[numEnzyme]) << " modifies ~ ";
		        	   	//reactant
		        		tempSp = Model_getSpeciesById(m, SimpleSpeciesReference_getSpecies(Reaction_getReactant(rxns[j], 0)));
		        		/*int k;
		        		for(k = 0; k < i; k++) {
						    if(strcmp(Species_getName(tempSp), Species_getName(s[k])) == 0)
					    	    break;
		        		}
		        		if(k==i) {
		        			k = addSubstrate(tempSp);
						}*/
						reactants[enzType] = tempSp; 
						addSubstrate(findIndexOfSpecies(tempSp));
		        		//cout << "REACTANT: " << Species_getName(reactants[enzType]) << "/" << Species_getName(tempSp) << "; ";
		        				        		
		        		//product
		        		tempSp = Model_getSpeciesById(m, SimpleSpeciesReference_getSpecies(Reaction_getProduct(rxns[j], 0)));
		        		/*for(k = 0; k < i; k++) {
						    if(strcmp(Species_getName(tempSp), Species_getName(s[k])) == 0)
					    	    break;
		        		}
		        		if(k==i) {
							k = addSubstrate(tempSp);
						}*/
		        		products[enzType] = tempSp; 
		        		//cout << "PRODUCT: ";
		        		addSubstrate(findIndexOfSpecies(tempSp));
		        		//cout << Species_getName(products[enzType]) << "/" << Species_getName(tempSp) << endl;
		        		
		        		//kinetic law
		        		KineticLaw * klTemp = Reaction_getKineticLaw(rxns[j]);
		        		if(klTemp!=NULL) {
			        		klaw[enzType] = Reaction_getKineticLaw(rxns[j]);
		        		}
		        		//cout << "klaw is null: " << (klaw[numEnzyme] == NULL) << endl;
		        		
		        		break; //won't be modifier for this reaction again.
		        	}
            	}
            	//cout << " Say what?" << endl;
		    }
            if(modifies) {
            	//cout << "numEnzyme incremented after " << Species_getName(e[i]) << endl;
            	numEnzyme++;
            } else {
            	addSubstrate(i);
            }
        } else {
        	//what about phenotypes, RNA, DNA etc??
        	//if(strstr(((string)SBase_getAnnotation(sp)).c_str(), "SIMPLE_MOLECULE")) {
				mets[i] = s[i];
				numMetabolite++;
			//}
        	addSubstrate(i);
        }
        //cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    }
    cout << numEnzyme << " enzymes and " << numSubstrate << " substrates" << endl; 
    inputFile.close();
    
    for (map<int, Species_t*>::const_iterator rs=reactants.begin(); rs!=reactants.end(); rs++) {
    	bool isProduct = false;
    	const char * nm = Species_getName(rs->second);
    	for (map<int, Species_t*>::const_iterator ps=products.begin(); ps!=products.end(); ps++) {
    		if(strcmp(nm, Species_getName(ps->second)) == 0) {
    			isProduct = true;
    		}
    	}
    	if(!isProduct && (protType.find((string)nm)==protType.end())) {
    		if(!initialMet.empty()) {
    			cout << "Uh oh! More than one initial metabolite." << endl;
    		}
    		//(*initialMet.begin()).second = (*rs).second;
    		initialMet[findIndexOfSpecies(rs->second)] = rs->second;
    	}
    }
    
    for (map<int, Species_t*>::const_iterator ps=products.begin(); ps!=products.end(); ps++) {
    	bool isReactant = false;
    	const char * nm = Species_getName(ps->second);
    	for (map<int, Species_t*>::const_iterator rs=reactants.begin(); rs!=reactants.end(); rs++) {
    		if(strcmp(nm, Species_getName(rs->second)) == 0) {
    			isReactant = true;
    			break;
    		}
    	}
    	map<string, int>::const_iterator type = protType.find((string)nm);
    	if(!isReactant) {
    		if (type==protType.end()) {
    			if(!finalMet.empty()) {
    				cout << "Uh oh! More than one final metabolite. " << Species_getName((*ps).second) << " replaces " << Species_getName((finalMet.begin())->second) << endl;
    			}
    			//(*finalMet.begin()).second = (*ps).second;
	    		finalMet[findIndexOfSpecies(ps->second)] = ps->second;
    		} else {
    			if(!finalSigEnzyme.empty()) {
    				cout << "Uh oh! More than one final signal molecule. " << Species_getName((*ps).second) << " replaces " << Species_getName((finalSigEnzyme.begin())->second) << endl;
    			}
	    		finalSigEnzyme[findIndexOfSpecies(ps->second)] = ps->second;
    		} 
    	} else {
    		if(type!=protType.end() && reactants.find(type->second)==reactants.end()) {
    			if(!finalSigEnzyme.empty()) {
    				cout << "Uh oh! More than one final signal molecule. " << Species_getName((*ps).second) << " replaces " << Species_getName((finalSigEnzyme.begin())->second) << endl;
    			}
	    		finalSigEnzyme[findIndexOfSpecies(ps->second)] = ps->second;
    		}
    	}
    }
    
	// Index that maps on to metabolites, since keys in "mets" are not consecutive numbers, from 0 to n
	// Needed for LatticeEnvironment function
	for(map<int, Species_t*>::const_iterator metI = mets.begin(); metI != mets.end(); metI++) {
		int val = metIndex.size();
		metIndex[metI->first] = val;
	}
	assert(metIndex.size() == numMetabolite);
}

int ParameterManager::findIndexOfSpecies(Species_t *sp) {
	int j;
	for (j = 0; j<numSpecies; j++) {
		if (strcmp(Species_getId(sp), Species_getId(s[j])) == 0) {
			break;
		}
	}
	assert(j<=numSpecies);
	return j;
}

//Check if substrate already exists; if not, add it to the list.
void ParameterManager::addSubstrate(int i) {
	//cout << "Adding substrate...";
	bool exists = false; 
	if(subs.find(i) != subs.end()) {
		exists = true;
		if(strcmp(Species_getId(s[i]), Species_getId(subs[i])) == 0) {
			//cout << "Substrate not added!" << endl;
			return;
		} else {
			cout << "Then what's this? " << Species_getName(subs[i]); 
		}
	}
	if(!exists) {
    	subs[i] = s[i];
    	//numMolecule[i] = Species_getInitialAmount(s[i]);
        //numMolSubstrate[numSubstrate] = Species_getInitialAmount(sp);
   	    numSubstrate++;
	}
	//cout << "Substrate added!" << endl;
}

//////////////////////////////////////////////////////////////////////
// Load parameters from data file 'fName'
//
void ParameterManager::load(string fName){
	string key;
	string value;
	string line;
	string delimiter = "=";
	
	// Open Parameter data file
	ifstream inFile (fName.c_str());
	
	if(inFile){

	// Populate the parameter table with data in the file
	while (!inFile.eof())
	{	
		getline(inFile,line,'\n');
		int keyPos = line.find(delimiter);		
		if(keyPos > 0){
			key = line.substr(0,keyPos);
			value = line.substr(keyPos + delimiter.length());
			
			// Add parameter to the paramater table
			parameterTable[key] = atoi(value.c_str());
		}
	}
	
	// Close Parameter data file
	inFile.close();
	}
}

