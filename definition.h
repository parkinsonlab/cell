#ifndef DEFINITION_H
#define DEFINITION_H

///////////////////////////////////////////////////////////////////
// Input File Paths
///////////////////////////////////////////////////////////////////

#define INPUT_FILE_ENV "input/env.in"
#define INPUT_FILE_DIFFUSION_RATES "input/diffusion_rates.in"
#define INPUT_FILE_PARAMS "input/params.in"
#define INPUT_FILE_ACTIVATION_NETWORK "input/activation_network.in"
#define INPUT_FILE_COMPATIBLE_ENV "input/compatible_env.in"

///////////////////////////////////////////////////////////////////
// Cell Environments
///////////////////////////////////////////////////////////////////

#define ENV_CYTOSOL 0
#define ENV_1 1
#define ENV_2 2
#define ENV_3 3
#define ENV_4 4

///////////////////////////////////////////////////////////////////
// Molecules Types
///////////////////////////////////////////////////////////////////

// Membranous Molecules
#define MOL_MEMBRANOUS_FIXED_PUMP	0
#define MOL_MEMBRANOUS_FIXED_GATE	1

// Intracellular Molecules
enum {
  MOL_ENZYME_A = 0,
  MOL_ENZYME_B,
  MOL_ENZYME_C,
  MOL_ENZYME_D,
  MOL_ENZYME_E,
  NUM_MOL_ENZYME
};

// Metabolites
enum {
  META_A = 0,
  META_B,
  META_C,
  META_D,
  META_E,
  NUM_META
};

///////////////////////////////////////////////////////////////////
// Molecule Names
///////////////////////////////////////////////////////////////////

// Membranous Molecules
#define MOL_NAME_MEM_FIXED_PUMP "Fixed Pump"
#define MOL_NAME_MEM_FIXED_GATE "Fixed Gate"

// Intracellular Molecules
#define MOL_NAME_ENZYME_A "Enzyme A"
#define MOL_NAME_ENZYME_B "Enzyme B"
#define MOL_NAME_ENZYME_C "Enzyme C"
#define MOL_NAME_ENZYME_D "Enzyme D"
#define MOL_NAME_ENZYME_E "Enzyme E"


///////////////////////////////////////////////////////////////////
// Molecule Group
///////////////////////////////////////////////////////////////////

#define MOL_GROUP_INTRACELLULAR 0
#define MOL_GROUP_MEMBRANOUS 1
#define MOL_GROUP_ENZYME 2

///////////////////////////////////////////////////////////////////
// Molecule Group Name
///////////////////////////////////////////////////////////////////

#define MOL_GROUP_NAME_INTRACELLULAR "Intracellular Molecule"
#define MOL_GROUP_NAME_MEMBRANOUS "Membranous Molecule"
#define MOL_GROUP_NAME_ENZYME "Metabolic Enzyme"

#define GATE_IS_CLOSED	0
#define GATE_IS_OPEN	1


typedef int moltype;
typedef int envtype;

#endif
