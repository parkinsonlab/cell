/*
===============================================================================

	FILE:  RandomNG.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Random Number Generator
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "RandomNG.h"


using namespace std;

int RandomNG :: inext;
int RandomNG :: inextp;
long RandomNG :: ma[56];
int RandomNG :: iff = 0;

RandomNG::RandomNG(){}

// Random integer value between min and mx (min <= value <= max)
int RandomNG :: randInt(int min, int max){

	if(min > max)	swap(min, max);
    
	int range = max - min + 1;
    
	return int(min + (orand() * range));
}

// Random float value between min and max (min < value <= max)
float RandomNG :: randFloat(float min, float max){

	if(min > max)	swap(min, max);
    
	float range = max - min;
    
	return min + range * orand();
    
}

// Negative Expontential
float RandomNG :: randNegExp(float val){
	return -val * log(orand()) ;
}

// Erlang
float RandomNG :: randErlang(float x, float s){

	int i, k;
	float z;

	z = x / s;
	k = (int) (z * z);
	for (i = 0, z = 1.0; i < k; i++)
		z *= orand();
	return -(x/k) * log(z);
}

// Normal Distribution with mean and standard deviation
float RandomNG :: randNormal(float mean,float stdev){
	float v1, v2, w, z1;

	do {
		v1 = 2.0 * orand() - 1.0;
		v2 = 2.0 * orand() - 1.0;
		w = v1 * v1 + v2 * v2;
	} while ( w >= 1.0 );

	w = sqrt( (-2.0 * log( w ) ) / w );
	z1 = v1 * w;

	return (mean + z1 * stdev);
}


// Get next Random float between 0.0 and 1.0
float RandomNG :: orand()
{

	long mj,mk;
	int i,ii,k;
	
	if(iff == 0){
		long idum = getSeed();
		
		iff=1;
		mj=MSEED-(idum < 0 ? -idum : idum);
		mj = abs(mj);
		mj %= MBIG;
		ma[55]=mj;
		mk=1;
		for(int i = 1 ; i <= 54 ; i++){
			int ii = (21 * i) % 55;
			ma[ii] = mk;
			mk = mj - mk;
			if (mk < MZ) mk += MBIG;
			mj = ma[ii];
		}
		for (int k = 1 ; k <= 4 ; k++)
			for(int i = 1 ; i <= 55 ; i++){
				ma[i] -= ma[1+(i+30) % 55];
				if (ma[i] < MZ) ma[i] += MBIG;
			}
			
		inext=0;
		inextp=31;
	}
	
	if(++inext == 56) inext=1;
	if(++inextp == 56) inextp=1;
	
	mj = ma[inext]-ma[inextp];
 	
	if(mj < MZ) mj += MBIG;
	
	ma[inext]=mj;
	return mj*(1.0/MBIG);
}


// Set the seed for the random sequence
long RandomNG :: getSeed(){

	long seed = static_cast<unsigned>(time(0)) * getpid();
	//printf("Seed\t%d\n", seed);
	return seed;
}
