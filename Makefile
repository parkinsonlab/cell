
#===============================================================================
# glowmech lesson 8
#===============================================================================

PROG=simulation

SRCDIR=.
GLOWDIR=../glow_src

MODULES=\
	Main \
	SimEngine \
	SimWindow \
	Simulation \
	GenSimulation \
	Molecule \
	IntracellularMolecule \
	Enzyme \
	MembranousMolecule \
	RandomNG \
	ParameterManager \
	LatticeSite \
	LatticeEnvironment \
	Point \
	Vector3D \
	Tuple \
	Matrix \
	Viewer

GLOWMODULES=\
	glow \
	glowAux \
	glowCheckBoxWidget \
	glowDebug \
	glowLabelWidget \
	glowMenuButtonWidget \
	glowPanelWidget \
	glowPushButtonWidget \
	glowQuickPalette \
	glowRadioButtonWidget \
	glowScrollBarWidget \
	glowSenderReceiver \
	glowSeparatorWidget \
	glowSliderWidget \
	glowTextData \
	glowTextFieldWidget \
	glowUtilities \
	glowVectorAlgebra \
	glowViewTransform \
	glowWidget

COMPILE=g++
COMPILE_IRIX=CC
SHELL=/bin/sh

LIBS=-lglut -lpthread -lGL -lGLU -lXi -lXmu -lX11 -lm -lstdc++ -lSM -lICE -lXext -lsbml
LIBS_IRIX=-lCio -lglut -lX11 -lXmu -lXext -lGL -lGLU -lm 

DEPFLAGS=-MM
DEPFLAGS_IRIX=-M

CFLAGS=-Wall \
	-DGLOW_PLATFORM_LINUX \
	-DGLOW_PLATFORM_LITTLEENDIAN \
	-DGRAPHICS \
	-ggdb \
    -I../../../sukwon/Downloads/libsbml-2.3.4/include/sbml #manual fix required 
    
NODEBUGCFLAGS=-O2 -DNODEBUG
LDFLAGS=-L/usr/local/lib -L/usr/X11R6/lib

CFLAGS_IRIX=\
	-fullwarn -no_auto_include \
	-LANG:std \
	-woff 1209,3201,1424,1375 \
	-OPT:Olimit=0 \
	-I../../Compat-SGI \
	-DGLOW_COMPAT_CLIBNOSTDNAMESPACE \
	-DGLOW_PLATFORM_IRIX \
	-DGLOW_PLATFORM_BIGENDIAN \
    -I../../../sukwon/Downloads/libsbml-2.3.4/include/sbml

LDFLAGS_IRIX=


include $(GLOWDIR)/glow_base.Makefile
