/*
===============================================================================

	FILE:  ParameterManager.h
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Parameter Manager
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef PMANAGER_H
#define PMANAGER_H


#include "common/extern.h"

#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <map>
#include <fstream>
#include <iostream>

#include "SBase.h"
#include "common/sbmlfwd.h"
#include "sbml/SBMLTypes.h"

#include "definition.h"

using namespace std;

class ParameterManager{

	public: 

		// Get an instance of the Parameter Manager
		static ParameterManager * getParameterManager();
		
		// Get value of the Parameter by the specified KEY
		int get(string);
		
		// Add parameter (key, value) pair
		void add(string, int);
		
		// load parameters from data file
		void load(string);
		
        // load SBML data file 
        void loadSBML(const char*);

	protected: 

		// Constructor for the Parameter Manager
		ParameterManager(); 

	private: 
	
		void addSubstrate(int);
		
		int findIndexOfSpecies(Species_t*);

		// Does the Parameter Manager already exist (Status)
		static bool isInstantiated;
		
		// Pointer to the Parameter Manager
		static ParameterManager * params;
		
		// Parameter Table	(key <-> value)
		map <string, int> parameterTable;
}
;

#endif
