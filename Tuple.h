/*
===============================================================================

	FILE:  Tupple.h
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Tuple
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef TUPLE_H
#define TUPLE_H

#include <math.h>

class Tuple{

	public:
		Tuple(int);
		Tuple(Tuple *);
		~Tuple();
		
		void add(Tuple *);
		void scalarize(float);
		void copyValue(Tuple *);
		void negate();
		void display();
		void multByElement(Tuple *);
		void flush();
		void divByElement(Tuple *);
		int getNumElements();
		float sum();
		float * tuple;

	private:
		int numElements;
};

#endif

