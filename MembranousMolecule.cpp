/*
===============================================================================

	FILE:  MembranousMolecule.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Membranous Molecule
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "MembranousMolecule.h"


//////////////////////////////////////////////////////////////////////
// Constructor for the MembranousMolecule object
//
MembranousMolecule::MembranousMolecule() : Molecule(){}
MembranousMolecule::MembranousMolecule(Point & loc) : Molecule(loc){}

//////////////////////////////////////////////////////////////////////
// Destructor for the MembranousMolecule object
//
MembranousMolecule::~MembranousMolecule(){

	// Destructing the MembranousMolecule
	printf("Deleting MembranousMolecule\n");
}



//////////////////////////////////////////////////////////////////////
// Get MembranousMolecule type
//
moltype MembranousMolecule::getType(){
	return type;
}


//////////////////////////////////////////////////////////////////////
// Get MembranousMolecule type name
//
string MembranousMolecule::getTypeName(){
	switch (type) {
		case MOL_MEMBRANOUS_FIXED_PUMP:
			return MOL_NAME_MEM_FIXED_PUMP;
			break;
		case MOL_MEMBRANOUS_FIXED_GATE:
			return MOL_NAME_MEM_FIXED_GATE;
			break;
	};
}


//////////////////////////////////////////////////////////////////////
// Get MembranousMolecule group
//
int MembranousMolecule::getGroup(){
	return MOL_GROUP_MEMBRANOUS;
}



//////////////////////////////////////////////////////////////////////
// Get MembranousMolecule group name
//
string MembranousMolecule::getGroupName(){
	return MOL_GROUP_NAME_MEMBRANOUS;
}


void MembranousMolecule :: move(){
	Vector3D translation(getTranslationVector());
	Point destination(pos.x + translation.x, pos.y + translation.y, pos.z + translation.z);
	int env = lEnv->getEnvironment(destination);
	
	if((env!=-1) && lEnv->isCompatibleEnvironment(getType(),env)){
		// Let molecule move within the lattice
		translate(translation);
	}
}

Vector3D MembranousMolecule :: getTranslationVector(){

	int dir = RandomNG::randInt(1,6);
	
	float i = 0.0;	float j = 0.0;	float k = 0.0;

	switch (dir){
		case 1:	i = 1.0;	break;
		case 2:	j = 1.0;	break;
		case 3:	i = -1.0;	break;
		case 4:	j = -1.0;	break;
		case 5:	k = 1.0;	break;
		case 6:	k = -1.0;	break;
	}
	
	Vector3D vec(i,j,k);
	return vec;
}
