/*
===============================================================================

	FILE:  SimEngine.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Simulation Engine
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "SimEngine.h"
#include <ctime>
#include <cstdlib>

extern SBMLDocument_t *d;
extern Model_t *m;
extern Species_t **s;
extern map<int, Species_t *> e;
extern map<int, Species_t *> subs;
extern map<int, Species_t *> prot;
extern map<int, Species_t *> mets;
extern string classInfo;
extern int numSpecies; 
extern int numEnzyme;
extern int numSubstrate;
extern int numProtein;
extern int numMetabolite;
extern multimap<int, string> molEnzyme;
extern multimap<int, string> molSubstrate;
//extern int *numMolEnzyme;
//extern int *numMolSubstrate;
extern int *numMolecule;
extern Reaction_t **rxns;
extern map<int, Species_t *> reactants;
extern map<int, Species_t *> products;
extern map<int, KineticLaw_t *> klaw;
extern map<int, Species_t *> initialMet;
extern map<int, Species_t *> finalMet;
extern map<int, Species_t *> finalSigEnzyme;
extern map<string, int> protType;
extern map<int, int> metIndex;

SimEngine :: SimEngine(){

	cell = GenSimulation::getSimulation();

	lEnv = LatticeEnvironment::getLatticeEnv();

	continueSimulation = false;
	showMetaConc = true;
	showMetaEnz = true;
	showEnv = false;
	showEnvCyt = false;
	showEnv1 = true;
	showEnv2 = true;
	showEnv3 = true;
	showEnv4 = true;
		
	r = new float [numMetabolite];
	g = new float [numMetabolite];
	b = new float [numMetabolite];
	
	for (int i=0; i<numMetabolite; i++) {
		switch(i) {
			case 0:
				r[i] = 1.0; g[i] = 0.0; b[i] = 0.0;
				break;
			case 1:
				r[i] = 1.0; g[i] = 1.0; b[i] = 0.0;
				break;
			case 2:
				r[i] = 0.0; g[i] = 1.0; b[i] = 0.0;
				break;
			case 3:
				r[i] = 0.0; g[i] = 1.0; b[i] = 1.0;
				break;
			case 4:
				r[i] = 0.0; g[i] = 0.0; b[i] = 1.0;
				break;
			case 5:
				r[i] = 1.0; g[i] = 0.0; b[i] = 1.0;
				break;
			case 6:
				r[i] = 1.0; g[i] = 1.0; b[i] = 1.0;
				break;
		}			
	}
}

SimEngine :: ~SimEngine(){
}

void
SimEngine :: simulate(){
	if(continueSimulation)
		if(!(cell->isSimulationCompleted())){
			cell->continueSimulation();
			//displayTotalConcentrations();
		}
}
#ifdef GRAPHICS
void
SimEngine :: setDisplayEnvironment(){
	// Set properties of the surface material
	GLfloat mat_ambient[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat mat_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
	GLfloat mat_specular[] = {0.0f, 0.0f, 0.0f, 0.5f};
	GLfloat mat_shininess[] = {50.0f};
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);

	glClearColor (0.0, 0.0, 0.0, 0.0);
	glShadeModel (GL_SMOOTH);
	
}

void SimEngine :: setDrawingColor(int type, bool act){
    int ps = prot.size();
    for(int i=0; i<ps; i++)
    {
        if(i == type)
        {
            float r = 0.0f, g = 0.0f, b = 0.0f;
            switch(i)
            {
                case 0:
                    r = 1.0f;
                    break;
                case 1:
                    g = 1.0f - 1/ps * i;
                    break;
                case 2:
                    b = 1.0f - 1/ps * i;
                    break;
                case 3:
                    r = 1.0f - 1/ps * i;
                    g = 1.0f - 1/ps * i;
                    break;
                case 4:
                    r = 1.0f - 1/ps * i;
                    b = 1.0f - 1/ps * i;
                    break;
                case 5:
                    g = 1.0f - 1/ps * i;
                    b = 1.0f - 1/ps * i;
                    break;
                case 6:
                    r = 1.0f - 1/ps * i;
                    g = 1.0f - 1/ps * i;
                    b = 1.0f - 1/ps * i;
                    break;
                default:
                    //r = 0.2f + 0.8 * rand() / (double) RAND_MAX;
                    //g = 0.2f + 0.8 * rand() / (double) RAND_MAX;
                    //b = 0.2f + 0.8 * rand() / (double) RAND_MAX;
                    r = 0.5f;
                    g = 0.5f;
                    b = 0.5f;
                    break;
            }
            
            glColor3f(r, g, b);
            break;
        }
    }
}

void
SimEngine :: drawMolecule(Molecule * mol){

	float xScale = (float)xDim/2;
	float yScale = (float)yDim/2;
	float zScale = (float)zDim/2;

	glPushMatrix();
	glTranslated(mol->pos.x-xScale,mol->pos.y-yScale,mol->pos.z-zScale);
	glutSolidSphere(0.5*resolution,8,8);
	glPopMatrix();
}

void
SimEngine :: displayEnvironment(){

	for(int i = 0 ; i < xDim ; i ++){
	for(int j = 0 ; j < yDim ; j ++){
	for(int k = 0 ; k < zDim ; k ++){
	
		bool display = false;
		float r = 0;
		float g = 0;
		float b = 0;
	
		Point p(i,j,k);
		LatticeSite * ls = lEnv->getLatticeSite(p);
		envtype env = ls->getEnvironment();
		
	//	if(env == showEnvType || showEnvType == -1){
		
		switch(env){
			case ENV_CYTOSOL:
				display = showEnvCyt;
				r = 0.1;
				break;
			case ENV_1:
				display = showEnv1;
				g = 0.1;
				r = 0.1;
				break;
			case ENV_2:
				display = showEnv2;
				g = 0.1;
				break;
			case ENV_3:
				display = showEnv3;
				g = 0.1;
				b = 0.1;
				break;
			case ENV_4:
				display = showEnv4;
				b = 0.1;
				break;
		}

		if(display){
			GLfloat ENV[] = {r, g, b, 0.0};
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, ENV);

			float xScale = (float)xDim/2;
			float yScale = (float)yDim/2;
			float zScale = (float)zDim/2;

			glPushMatrix();
			glTranslated(i-xScale,j-yScale,k-zScale);
			glutSolidSphere(0.5,8,8);
			glPopMatrix();
		}
		
	//	}
	}
	}
	}
}

void SimEngine :: displayMetaboliteConcentration(){

  const static float CONCENTRATION_DISPLAY_FACTOR = 1.0e2;

	Matrix * state = lEnv->getStateMatrix();

	float xScale = ((float)xDim)/2;
	float yScale = ((float)yDim)/2;
	float zScale = ((float)zDim)/2;

	float max = 0.2;
	float twicemax = max * 2.0;
	float thricemax = max * 3.0;

	for(int i = 0 ; i < xDim ; i ++){
	for(int j = 0 ; j < yDim ; j ++){
	for(int k = 0 ; k < zDim ; k ++){

		Point p(i,j,k);
		LatticeSite * ls = lEnv->getLatticeSite(p);
		int id = ls->getID();

		Tuple * t = state->getRow(id);
		float conc;

		float intensity[3];

        if(concType != -1)
        {
            //for(int l=0; l<numSubstrate; l++)
            //{
                intensity[0] = 0.0f;
                intensity[1] = 0.0f;
                intensity[2] = 0.0f;
                
                switch(concType%7)
                {
                    case 0:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[0] = 1.0f - 1/numSubstrate * concType;
                        intensity[0] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    case 1:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[1] = 1.0f - 1/numSubstrate * concType;
                        intensity[1] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    case 2:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[2] = 1.0f - 1/numSubstrate * concType;
                        intensity[2] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    case 3:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[0] = 1.0f - 1/numSubstrate * concType;
                        //intensity[1] = 1.0f - 1/numSubstrate * concType;
                        intensity[0] = (conc > 1.0) ? 1.0 : conc;
                        intensity[1] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    case 4:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[0] = 1.0f - 1/numSubstrate * concType;
                        //intensity[2] = 1.0f - 1/numSubstrate * concType;
                        intensity[0] = (conc > 1.0) ? 1.0 : conc;
                        intensity[2] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    case 5:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[1] = 1.0f - 1/numSubstrate * concType;
                        //intensity[2] = 1.0f - 1/numSubstrate * concType;
                        intensity[1] = (conc > 1.0) ? 1.0 : conc;
                        intensity[2] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    case 6:
                        conc = t->tuple[concType] / twicemax;
                        conc *= CONCENTRATION_DISPLAY_FACTOR;
                        //cout << conc << endl;
                        //intensity[0] = 1.0f - 1/numSubstrate * concType;
                        //intensity[1] = 1.0f - 1/numSubstrate * concType;
                        //intensity[2] = 1.0f - 1/numSubstrate * concType;
                        intensity[0] = (conc > 1.0) ? 1.0 : conc;
                        intensity[1] = (conc > 1.0) ? 1.0 : conc;
                        intensity[2] = (conc > 1.0) ? 1.0 : conc;
                        break;
                    default:
                        break;
                } 
                        if (intensity[0] > 0.01 || intensity[1] > 0.01 || intensity[2] > 0.01) {
                            GLfloat METABOLITECONC[] = {intensity[0], intensity[1], intensity[2], 0.0};
                            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, METABOLITECONC);
                            
                            glPushMatrix();
                            glTranslated(i-xScale,j-yScale,k-zScale);
                            glutSolidCube(1.0);
                            glPopMatrix();
                        }
                        
                
            //}
        }
        if(concType == -1)
        {
            intensity[0] = 0.0f;
            intensity[1] = 0.0f;
            intensity[2] = 0.0f;

            int count1 = 0, count2 = 0, count3 = 0;
            float *concn = new float [numMetabolite];
            int numFirstSet = (numMetabolite + 1) / 3;
            for(int m=0; m<numMetabolite; m++)
            {
                concn[m] = t->tuple[m];
                concn[m] *= CONCENTRATION_DISPLAY_FACTOR;
                
                //intensity[m%3] += concn[m]; 
                //switch(m%3)
                //{
                //    case 0:
                //        count1++;
                //        break;
                //    case 1:
                //        count2++;
                //        break;
                //    case 2:
                //        count3++;
                //        break;
                //}
            }

            for(int m=0; m<numFirstSet; m++)
            {
                intensity[0] += concn[m];
            }

            for(int m=numFirstSet-1; m<numFirstSet*2; m++)
            {
                intensity[1] += concn[m];
            }

            for(int m=numFirstSet*2-1; m<numMetabolite; m++)
            {
                intensity[2] += concn[m];
            }

            intensity[0] /= numFirstSet * max;
            intensity[1] /= (numFirstSet+1) * max;
            intensity[2] /= (numMetabolite - numFirstSet*2 + 1) * max;
            
            //intensity[0] /= (count1+1);
            //intensity[1] /= (count2+1);
            //intensity[2] /= (count3+1);
            
            if (intensity[0] > 0.01 || intensity[1] > 0.01 || intensity[2] > 0.01) {
			GLfloat METABOLITECONC[] = {intensity[0], intensity[1], intensity[2], 0.0};
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, METABOLITECONC);

			glPushMatrix();
			glTranslated(i-xScale,j-yScale,k-zScale);
			glutSolidCube(1.0);
			glPopMatrix();
        }
        }
    }
    }
    }
}



void
SimEngine :: displayTotalConcentrations(){
	Matrix * state = lEnv->getStateMatrix();
	Tuple * t = state->collapse();
	t->display();
	delete t;
}

void
SimEngine :: plotMolecules(){
	map<int, vector <Molecule *> > * mTable = cell->getMoleculeTable();

	// Iterate the table of molecules that are in the lattice
	map<int, vector <Molecule *> >::const_iterator itr = mTable->begin();

	// For each type of molecules in the lattice
	for (itr = mTable->begin(); itr != mTable->end(); itr ++)
	{
		for(int i = 0 ; i < (signed)(itr->second).size() ; i ++){
			Molecule * m = (itr->second)[i];
			glDisable(GL_LIGHTING);
			setDrawingColor(m->getType(), m->isActivated());
			drawMolecule(m);
			glEnable(GL_LIGHTING);
		}
	}

	map<int, vector <MembranousMolecule *> > * mmTable = cell->getMembranousMoleculeTable();

	// Iterate the table of molecules that are in the lattice
	map<int, vector <MembranousMolecule *> >::const_iterator mItr;

	for (mItr = mmTable->begin(); mItr != mmTable->end(); mItr++)
	{
		for (int i = 0; i < (signed)(mItr->second).size() ; i++) {
			MembranousMolecule * m = (mItr->second)[i];
			glDisable(GL_LIGHTING);
			glPushMatrix();
			float maxDim = max(xDim, max(yDim, zDim));
			glTranslatef(m->pos.x-xDim/2,
				     m->pos.y-yDim/2,
                                     (float) m->pos.z - (float) (zDim/2));

			if (m->getType() == MOL_MEMBRANOUS_FIXED_GATE)
				glColor3f( 0.8f, 0.0f, 0.8f );
			else
				glColor3f( 0.0f, 0.8f, 0.8f );

			glScaled(0.4, 0.4, 1.4);
			glutSolidCube(1.0);
			glPopMatrix();
			glEnable(GL_LIGHTING);
		}
	}
}

void
SimEngine :: drawCellMembrane(){
	// Construct the Cell Volume
	glPushMatrix();
	int maxDim = max(xDim,max(yDim,zDim));
	glScaled(((float)xDim)/maxDim,((float)yDim)/maxDim,((float)zDim)/maxDim);
	glTranslated(-0.5, -0.5, -0.5);
	glDisable(GL_LIGHTING);
	glColor3f(0.5,0.5,0.5);
	glutWireCube(maxDim);
	glEnable(GL_LIGHTING);
	glPopMatrix();

	GLfloat outwardFace[] = {0.0, 0.25, 0.25, 0.0};
	GLfloat inwardFace[]  = {0.0, 0.10, 0.10, 0.0};

	struct LatticeMacroFeature * curMacroFeature = lEnv->getLatticeMacroFeatures();

	while (curMacroFeature) {
		if (curMacroFeature->envType != ENV_1) {
			curMacroFeature = curMacroFeature->next;
			continue;
		}

		if (curMacroFeature->featureType == LATTICE_XPLANE) {
			glPushMatrix();
			glTranslated(-0.5, -0.5, -0.5);

			// There are two leaves to the membrane plane; these will be drawn
			// so that each lights up more strongly on its outward facing side
			glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, inwardFace);
			glMaterialfv(GL_BACK,  GL_AMBIENT_AND_DIFFUSE, outwardFace);

			glBegin( GL_POLYGON );
			glVertex3f( curMacroFeature->x1 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0 );
			glVertex3f( curMacroFeature->x1 + 1.0 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0 );
			glVertex3f( curMacroFeature->x1 + 1.0 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0 );
			glVertex3f( curMacroFeature->x1 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0  - zDim/2.0 );
			glEnd();

			glMaterialfv(GL_BACK,  GL_AMBIENT_AND_DIFFUSE, inwardFace);
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, outwardFace);

			glBegin( GL_POLYGON );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0);
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0);
			glEnd();

			// be sure to restore the former light model if you don't want
			// a fireworks display
			glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
			glPopMatrix();
		}
		else if (curMacroFeature->featureType == LATTICE_YPLANE) {
			glPushMatrix();
			glTranslated(-0.5, -0.5, -0.5);

			// There are two leaves to the membrane plane; these will be drawn
			// so that each lights up more strongly on its outward facing side
			glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, inwardFace);
			glMaterialfv(GL_BACK,  GL_AMBIENT_AND_DIFFUSE, outwardFace);

			glBegin( GL_POLYGON );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0 );
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0 );
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 + 1.0 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0 );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 + 1.0 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0 );
			glEnd();

			glMaterialfv(GL_BACK,  GL_AMBIENT_AND_DIFFUSE, inwardFace);
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, outwardFace);

			glBegin( GL_POLYGON );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0);
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z2 + 1.0 - zDim/2.0);
			glEnd();

			// be sure to restore the former light model if you don't want
			// a fireworks display
			glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
			glPopMatrix();
		}
		else if (curMacroFeature->featureType == LATTICE_ZPLANE) {
			glPushMatrix();
			glTranslated(-0.5, -0.5, -0.5);

			// There are two leaves to the membrane plane; these will be drawn
			// so that each lights up more strongly on its outward facing side
			glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, inwardFace);
			glMaterialfv(GL_BACK,  GL_AMBIENT_AND_DIFFUSE, outwardFace);

			glBegin( GL_POLYGON );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 + 1.0 - zDim/2.0 );
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 + 1.0 - zDim/2.0 );
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 + 1.0 - zDim/2.0 );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 + 1.0 - zDim/2.0 );
			glEnd();

			glMaterialfv(GL_BACK,  GL_AMBIENT_AND_DIFFUSE, inwardFace);
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, outwardFace);

			glBegin( GL_POLYGON );
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y1 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x2 + 1.0 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glVertex3f( curMacroFeature->x1 - xDim/2.0,
                                    curMacroFeature->y2 + 1.0 - yDim/2.0,
                                    curMacroFeature->z1 - zDim/2.0);
			glEnd();

			// be sure to restore the former light model if you don't want
			// a fireworks display
			glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
			glPopMatrix();
		}

		curMacroFeature = curMacroFeature->next;
	}
}

void
SimEngine :: constructLattice(){

	glEnable (GL_BLEND);
	glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	//glBlendFunc (GL_ONE_MINUS_SRC_ALPHA, GL_ONE);

	glPushMatrix();

	if(showMetaEnz)
		plotMolecules();

	drawCellMembrane();

	if(showMetaConc)
		displayMetaboliteConcentration();
	
	if(showEnv)	
		displayEnvironment();

	glPopMatrix();

	glDisable (GL_BLEND);
}

void
SimEngine :: displayStats(){

	Matrix * stats = lEnv->getStatisticsMatrix();
        int num = stats->getNumRows();

	Tuple * t = stats->getRow(num-1);

	float max = 800;
	float one = 1;
	
	float length = 50.0;
	float factor = 1;
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslated(0.0, 0.0, -20.0);
	glTranslated(0.0,-7.0,0.0);
	
	glDisable(GL_LIGHTING);
	
	for(int i = 0 ; i < numMetabolite ; i ++){
		glPushMatrix();
		glColor3f(r[i],g[i],b[i]);
		factor = 1.0 * min(t->tuple[i]/max,one) * length;
		glScaled(factor,1.0,1.0);
		glutSolidCube(0.2);
		glPopMatrix();
	
		glTranslated(0.0,-0.5,0.0);
	}
	glEnable(GL_LIGHTING);
}

void 
SimEngine :: displayGraph(){

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
 	gluOrtho2D( 0.0, 6500.0, 0.0, 6500.0 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef( 25.0, 50.0, 0.0 );
	glDisable(GL_LIGHTING);

	string metName;
	for(map<int, int>::const_iterator itr = metIndex.begin(); itr!=metIndex.end(); itr++) {
		metName = Species_getName(s[itr->first]);
		glColor3f(r[itr->second], g[itr->second], b[itr->second]);
		for (int abc=0; abc < metName.length(); abc++) {
			glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, metName[abc]);
		}
		glColor3f(1.0, 1.0, 1.0);
		glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, '|');
	}		
	
	glEnable(GL_LIGHTING);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-0.5, 0.5, -0.5, 0.5, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslated(0.0, 0.0, -40.0);
	
	float x = -16;
	float y = -12.5;
	float height = 5.0;

	glDisable(GL_LIGHTING);
	
	glPointSize(2);
	
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_LINES);
		glVertex3f(x,y+height,0.0);
		glVertex3f(x,y,0.0);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(x,y,0.0);
		glVertex3f(-x,y,0.0);
	glEnd();
		
	float alter = 0;
		
	Matrix * stats = lEnv->getStatisticsMatrix();
	int numCyc = stats->getNumRows();
	float one = 1;
	float max = 800;
		
	Tuple * conc;
	float *concs = new float [numMetabolite];
		
	for(int i = 0 ; i < numCyc ; i ++){
		
		conc = stats->getRow(i);
		
		/*float concA = min(conc->tuple[0]/max,one);
		float concB = min(conc->tuple[1]/max,one);
		float concC = min(conc->tuple[2]/max,one);
		float concD = min(conc->tuple[3]/max,one);
		float concE = min(conc->tuple[4]/max,one);*/
		for(int j=0; j<numMetabolite; j++) {
			concs[j] = min(conc->tuple[j]/max,one);
		}
		
        //cout << "conc->tuple[0]: " << conc->tuple[0] << endl;
        //cout << "conc->tuple[1]: " << conc->tuple[1] << endl;
        //cout << "conc->tuple[2]: " << conc->tuple[2] << endl;
        //cout << "conc->tuple[3]: " << conc->tuple[3] << endl;
        //cout << "conc->tuple[4]: " << conc->tuple[4] << endl;

        //cout << "concA: " << concA << endl;
        //cout << "concB: " << concB << endl;
        //cout << "concC: " << concC << endl;
        //cout << "concD: " << concD << endl;
        //cout << "concE: " << concE << endl;

		glBegin(GL_POINTS);
			
			for(int j=0; j<numMetabolite; j++) {
				glColor3f(r[j],g[j],b[j]);
				glVertex3f(x+alter,y+5*(concs[j]*height),0.0f);
				
				if(j==metIndex[(finalMet.begin())->first]) {
					alter+=0.02;
				}
			}
			
			/*glColor3f(1.0,1.0,0.0);
			glVertex3f(x+alter,y+500*(concB*height),0.0f);
			
			glColor3f(0.0,1.0,0.0);
			glVertex3f(x+alter,y+500*(concC*height),0.0f);
			
			glColor3f(0.0,1.0,1.0);
			glVertex3f(x+alter,y+500*(concD*height),0.0f);
			
			glColor3f(0.0,0.0,1.0);
			glVertex3f(x+alter,y+500*(concE*height),0.0f);
			alter+=0.02;*/
			
		glEnd();
	}
		
	glEnable(GL_LIGHTING);
}

void SimEngine :: displayNumCycles() {
 	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
 	gluOrtho2D( 0.0, 6500.0, 0.0, 6500.0 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef( 25.0, 50.0, 0.0 );
	glDisable(GL_LIGHTING);
	glColor3f( 0.0, 0.8, 1.0 );

	char buf[80];
	sprintf( buf, "%d", cell->getCycles() );

	int i = 0;
	while (buf[i] != '\0') {
		glutStrokeCharacter( GLUT_STROKE_MONO_ROMAN, buf[i] );
		i++;
	}
}


void SimEngine :: display(){
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);

	// the following four lines of code are a HACK to
	// get the membrane planes lighting to work properly;
	// I have no idea why it works and why particular
	// parametric values are required to glutSolidSphere to
	// make it work
	glPushMatrix();
	glTranslated(1000.0,1000.0,1000.0);
	glutSolidSphere(1.0,8,8);
	glPopMatrix();

	glPushMatrix();
	constructLattice();
	glPopMatrix();	

//	glPushMatrix();	
//	displayStats();
//	glPopMatrix();

	displayNumCycles();
	
	glFlush();
	Glow::SwapBuffers();
}
#endif /* GRAPHICS */
