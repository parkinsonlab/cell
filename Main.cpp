/*
===============================================================================

	FILE:  Main.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Main 
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh, John Parkinson
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh, John Parkinson>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

/*
===============================================================================
	Headers
===============================================================================
*/
#include "common/extern.h"
#include <iostream>
#include <string>

#include "SBase.h"
#include "common/sbmlfwd.h"
#include "sbml/SBMLTypes.h"

using namespace std;

#ifdef GRAPHICS
// These are the GLOW headers used by this program
#include "glow.h"
#include "glowLabelWidget.h"
#include "glowPushButtonWidget.h"
#include "glowRadioButtonWidget.h"
#include "glowCheckBoxWidget.h"
#include "glowTextFieldWindow.h"
#include "glowViewTransform.h"
#include <GL/glut.h>
#include <GL/gl.h>

GLOW_NAMESPACE_USING
#endif /* GRAPHICS */

#include <stdio.h>
#include <string>
#include <map>
#include <vector>

#include "Molecule.h"
#include "Matrix.h"
#include "GenSimulation.h"
#include "functionLib.h"
#include "ParameterManager.h"
#include "LatticeEnvironment.h"
#include "SimEngine.h"

#ifdef GRAPHICS
#include "SimWindow.h"
#endif /* GRAPHICS */
ParameterManager * pm = ParameterManager::getParameterManager();

// Dimensions of the Lattice
int xDim = pm->get("X_DIM");
int yDim = pm->get("Y_DIM");
int zDim = pm->get("Z_DIM");

int numLatticeSites = xDim * yDim * zDim;
int numEnvironments= 5;
float resolution = 0.25;

double TIMESCALE = 5.0e-6;
/* The length of time that one timestep represents, in seconds */

double SPACESCALE = 5.0e-8; /* 10^-7 sized lattice cells means that 10 of them makes up about the diameter of an E. coli cell */
/* The distance between neighbouring lattice cells, in metres */

int concType = -1;
int showEnvType = -1;

//// SBML library variables
SBMLDocument_t *d;
Model_t *m;
Species_t **s;		// Total species(Enzymes + Substrates)
map<int, Species_t *> e;		// Enzyme
map<int, Species_t *> subs;	// Substrates
map<int, Species_t *> prot;	// Proteins
map<int, Species_t *> mets;	// Metabolites
Reaction_t **rxns;	// Reactions
map<int, Species_t *> reactants;
map<int, Species_t *> products;
map<int, KineticLaw_t *> klaw;
map<int, Species_t *> initialMet;
map<int, Species_t *> finalMet;
map<int, Species_t *> finalSigEnzyme;
map<string, int> protType;
map<int, int> metIndex;

string classInfo;
char *checkMoleculeType = NULL;
int numSpecies=0;
int numEnzyme=0;
int numSubstrate=0;
int numMetabolite=0;
int numProtein=0;
//int *molEnzyme;     // Intracellular Molecule numbers
//int *molSubstrate;  // Metabolites numbers
multimap<int, string> molEnzyme;
multimap<int, string> molSubstrate;
//int *numMolEnzyme;
//int *numMolSubstrate;
int *numMolecule;
char **nameEnzymes;

SimEngine * simulation;
#ifdef GRAPHICS
SimDisplayWindow * sub;
StatsWindow * stats;
SimWindow * simWin;
#endif /* GRAPHICS */

void simulate(){

	simulation->simulate();
	if(simulation->continueSimulation){
#ifdef GRAPHICS
		sub->Refresh();
		stats->Refresh();
#endif /* GRAPHICS */
		// Pause simulation every specific number of cycles
//		if(simulation->cell->getCycles() % 5 == 0){
//			simWin->cb_continueSimulation->SetState(GlowCheckBoxWidget::off);
//			simulation->continueSimulation = false;
//		}
	}
}

/*
===============================================================================
	Entry point
===============================================================================
*/


int main(int argc, char **argv)
{
	bool graphicsArg = false;

	//if(argc != 3) {
	//   cout << "Incorrect number of argument!\n";
	//    exit(1);
	//}
	for (int i = 1; i < argc; i++) {
        if (strstr(argv[i], ".xml") != NULL) {
            pm->loadSBML(argv[i]);
        }
        else {
        	cout << "Please provide an SBML document to read!\n";
        }
	}

    // by Sukwon define enumeration for all the enzymes
    ///////////////////////////////////////////////////
    // Intracellular Molecules enumeration
    
    //molEnzyme = new int[numEnzyme];
    int index=0;
    for(map<int, Species_t*>::const_iterator enz = prot.begin(); enz!=prot.end(); enz++) {
        //molEnzyme[i] = i;
        char * nameEnzyme = (char *) Species_getName(enz->second);
        
        molEnzyme.insert(pair<int, string> (index, (string)nameEnzyme));
        index++;
    }

    //molSubstrate = new int[numSubstrate];
    
    // Metabolite Molecules enumeration
    index = 0;
    for(map<int, Species_t*>::const_iterator met = mets.begin(); met!=mets.end(); met++)
    {
        char * nameSubstrate = (char *) Species_getName(met->second);

        molSubstrate.insert(pair<int, string> (index, (string)nameSubstrate));
        //molSubstrate[i] = i;
        index++;
    }
    
    for (int i = 1; i < argc; i++)
        if (strcmp(argv[i], "-g") == 0)
            graphicsArg = true;
	
	simulation = new SimEngine();
	
	if (graphicsArg) {
		#ifdef GRAPHICS
			simWin = new SimWindow();
			simWin->setup();
	
			Glow::MainLoop();
		#endif /* GRAPHICS */
	}
	else {
		simulation->continueSimulation = true;
		while (1) { simulation->simulate(); }
	}

	return 0;
}
