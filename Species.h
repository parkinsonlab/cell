/*
===============================================================================

	FILE:  Species.h
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Parameter Manager
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef Species_H
#define Species_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>

//#include "Molecule.h"
//#include "RandomNG.h"

#include "SBase.h"
#include "common/sbmlfwd.h"
#include "sbml/SBMLTypes.h"

#include "definition.h"
#include "functionLib.h"
#include "IntracellularMolecule.h"

class Species {

	public:
	
		//////////////////////////////////////////////////////////////////////
		// Constructor and Destructor
		Species(Species_t*);
		Species(Species_t*, string);
		virtual ~Species();

		//////////////////////////////////////////////////////////////////////
		// References to other Species		
				
		Species_t* get();
		string getType();
		string getReactant();
		string getProduct();
		int getNumModifiers;
		string getModifier(int);
		string getPrev();
		string getNext();
		
		void setReactant(string);
		void setProduct(string);
		void setModifier(string);
		void setPrev(string);
		void setNext(string);

	protected:

		Species_t* orig;
		string type;
		string reactant;
		string product;
		int numModifiers;
		string * modifier;
		string prev;
		string next;
		
		void determineType();
	
	private:

		bool areSame(string, string);
			
}
;

#endif
