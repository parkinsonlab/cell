use strict;

### Run Simulations repetitively to generate statistical data
my $percent_inaccessible_value = 0;

my ($dim, $num_molMemR, $num_molA, $num_molB, $num_molC, $num_molNucR, $i);
my ($workDir, $outDir, $output, $f_qsub, $params, $stdoutPath, $p_file);
my ($queueQuotaFull, $numProcessesOnQueue, $queueStat);

for($i = 0 ; $i < 100 ; $i ++){
    
    print "$percent_inaccessible_value\n";
    
    $workDir = "/projects/jparkins/csanford/Microtubules/Particles/0/";
    $outDir = "/projects/jparkins/csanford/Microtubules/Particles/out/";
    $output = $outDir.$percent_inaccessible_value.".out";
    $f_qsub = $workDir."queue/sim_".$percent_inaccessible_value.".qs";
    $stdoutPath = $outDir."stdout/";
    
    if(-f "$output"){       system("rm $output");}
    
    &waitUntilQueueAvailable();
    
    open(qsub_file,"> $f_qsub");
    print qsub_file "#! /usr/local/bin/bash\n";
    print qsub_file "#PBS -l cput=3:00:00\n";
    print qsub_file "#PBS -l ncpus=1\n";
    print qsub_file "PROGRAM=$workDir"."simulation\n";
    print qsub_file "OUTPUT=$output$i\n";
    print qsub_file "\$PROGRAM > \$OUTPUT\n";
    close(qsub_file);
    
    system("qsub -o $stdoutPath -e $stdoutPath $f_qsub");
    system("rm $f_qsub");
    sleep(1);
}

sub waitUntilQueueAvailable(){

        $queueQuotaFull=1;

        # Assume Queue is Full, then check availability
        while($queueQuotaFull == 1){
                $queueStat = `/usr/pbs/bin/qstat`;
                wait();

                $numProcessesOnQueue = 0;

                while($queueStat =~/mlkyip/g){
                        $numProcessesOnQueue++;
                }

                # If the number of processes in the queue reached the quota
                if($numProcessesOnQueue >= 25){
                        # Sleep for 2 minutes then check availability again
                        sleep(120);
                }
                else{
                        # The Queue is not full
                        $queueQuotaFull = 0;
                }
        }
}
