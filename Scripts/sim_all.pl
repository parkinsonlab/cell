#! /usr/bin/perl

my @percents = (0, 5, 10, 20, 50);

my $iter;

$iter = 0;

while (true)
{
    foreach my $percent (@percents)
    {
	system "cp input/params.in.template input/params.in";
	system "echo INACCESSIBLE_SPACE_PERCENT=$percent >> input/params.in";
	system "Scripts/sim_all $percent.$iter";
    }

    $iter ++;
}
