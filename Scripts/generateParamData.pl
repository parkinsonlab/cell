# Hashtable : KEY -> VALUE
%table;

# Molecule Table (Aliasing Molecules Name by ID)
$table{"MOL_ENZYME_A"} = 0;
$table{"MOL_ENZYME_B"} = 1;
$table{"MOL_ENZYME_C"} = 2;
$table{"MOL_ENZYME_D"} = 3;

# Environment Table (Aliasing Environments by ID)
$table{"ENV_CYTOSOL"} = 0;
$table{"ENV_1"} = 1;
$table{"ENV_2"} = 2;
$table{"ENV_3"} = 3;
$table{"ENV_4"} = 4;

# Directory from work directory
$dir = "./input";

# Data files to be generated
$f_activation_network = $dir."/activation_network.in";
$f_compatible_env = $dir."/compatible_env.in";
$f_diffusion_rates = $dir."/diffusion_rates.in";
$f_lattice_env_pl = $dir."/lattice_env_pl.in";
$f_lattice_env = $dir."/lattice_env.in";
$f_num_molecules = $dir."/num_molecules.in";
#$f_gen_env_pl = $dir."/gen_env.pl";

# Open data files to write
open(activation_network, ">$f_activation_network");
open(compatible_env, ">$f_compatible_env");
open(lattice_env_pl, ">$f_lattice_env_pl");
open(lattice_env, ">$f_lattice_env");
open(num_molecules, ">$f_num_molecules");
open(diffusion_rates, ">$f_diffusion_rates");

# Read input file "parameters.in"
open (PARAMS, "parameters.in");
	while($line = <PARAMS>){
	
		# Build Activation Network
		if($line =~ /^AN\t/){
			chomp $line;
			@entries = split(/\t/,$line);
			$activator = $table{$entries[1]};
			for($num = 2; $num < scalar(@entries) ; $num++){
				$activatee = $table{$entries[$num]};
				$output = "$activator\t$activatee\n";
				print(activation_network $output);
			}
		}
		
		# Build Molecule Compatible Environment
		elsif($line =~ /^CE\t/){
			chomp $line;
			@entries = split(/\t/,$line);
			$molecule = $table{$entries[1]};
			for($num = 2; $num < scalar(@entries) ; $num++){
				$com_env = $table{$entries[$num]};
				$output = "$molecule\t$com_env\n";
				print(compatible_env $output);
			}
		}
		
		# Build Lattice Environment
		elsif($line =~ /^LD\t/){
			chomp $line;
			@entries = split(/\t/,$line);
			$dim = $entries[1];
			$value = $entries[2];
			$output = $dim."=".$value;
			print(lattice_env_pl "\$".$output.";\n");
			print(lattice_env $output."\n");
		}
		
		# Initialize Molecules
		elsif($line =~ /^NM\t/){
			chomp $line;
			@entries = split(/\t/,$line);
			$molecule = $entries[1];
			$number = $entries[2];
			$output = "NUM_".$molecule."=".$number."\n";
			print(num_molecules $output);
		}

		elsif($line =~ /^DR\t/){
			chomp $line;
			@entries = split(/\t/,$line);
			$from = $table{$entries[1]};
			$to = $table{$entries[2]};
			print(diffusion_rates "$from\t$to");
			for($num = 3 ; $num < scalar(@entries); $num++){
				$output = "\t".$entries[$num];
				print(diffusion_rates $output);
			}
			print(diffusion_rates "\n");
		}
	}

# Close Data files after write
close(PARAMS);
close(activation_network);
close(compatible_env);
close(lattice_env_pl);
close(lattice_env);
close(num_molecules);
close(diffusion_rates);

# Clean up temporary files and merge data files
#system("cat $f_lattice_env_pl ./perlScripts/env_template.pl > $f_gen_env_pl");
#system("perl $f_gen_env_pl > $dir\/env.in");
system("cat $f_lattice_env $f_num_molecules > $dir\/params.in");
system("rm $f_lattice_env $f_lattice_env_pl $f_num_molecules $f_gen_env_pl");

#system("./simulation g");
