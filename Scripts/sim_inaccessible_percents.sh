for X in 0 5 10 25 50 80
do
  echo Simulating $X% inaccessible sample \#$1
  cp -f input/params.in.template input/params.in
  echo INACCESSIBLE_SPACE_PERCENT=$X >> input/params.in
  ./simulation > output/$X.$1.csv
done
