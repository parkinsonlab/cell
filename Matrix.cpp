/*
===============================================================================

	FILE:  Matrix.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Matrix 
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "Matrix.h"
#include <stdio.h>

Matrix :: Matrix(Tuple * t){

//printf("Matrix Created:\t%d\n",(long)this);
	matrix.push_back(t);

	numRows = matrix.size();
	numCols = t->getNumElements();
}

Matrix :: Matrix(int rows, int cols){
//printf("Matrix Created:\t%d\n",(long)this);
	for(int r = 0 ; r < rows ; r ++){
		Tuple * t = new Tuple(cols);
		matrix.push_back(t);
	}

	numRows = rows;
	numCols = cols;
}

Matrix :: ~Matrix(){
//printf("Deleting Matrix:\t%d\n",(long)this);
	for(int i = 0 ; i < numRows ; i ++){
		delete matrix[i];
	}
}

void Matrix :: add(Matrix * m){
	if(numRows == m->getNumRows() && numCols == m->getNumCols()){
		for(int row = 0 ; row < numRows ; row++){
			matrix[row]->add((m->matrix)[row]);
		}
	}
}

void Matrix :: addAndClear(Matrix * m){
	if(numRows == m->getNumRows() && numCols == m->getNumCols()){
		for(int row = 0 ; row < numRows ; row++){
			matrix[row]->add((m->matrix)[row]);
			(m->matrix)[row]->flush();
		}
	}
}

void Matrix :: scalarize(float s){
	for(int row = 0 ; row < numRows ; row++){
		matrix[row]->scalarize(s);
	}
}

void Matrix :: addValueToRow(int row, Tuple * addRow){
	matrix[row]->add(addRow);
}

void Matrix :: copyValueToRow(int row, Tuple * copyRow){
	matrix[row]->copyValue(copyRow);
}

Tuple * Matrix :: collapse(){
	Tuple * t = new Tuple(numCols);
	for(int col = 0 ; col < numCols ; col ++){
		float sum = 0;
		for(int row = 0 ; row < numRows ; row ++){
			sum += matrix[row]->tuple[col];
		}
		t->tuple[col] = sum;
	}
	return t;
}

Tuple * Matrix :: getRow(int rowNum){
	if(rowNum < numRows){
		return matrix[rowNum];
	}
}

int Matrix :: getNumRows(){
	return numRows;
}

int Matrix :: getNumCols(){
	return numCols;
}

void Matrix :: display(){
	for(int i = 0 ; i < numRows ; i ++){
		Tuple * t = matrix[i];
		for(int j = 0 ; j < numCols ; j ++){
			printf("%f\t",t->tuple[j]);
		}
		printf("\n");
	}
}

float Matrix :: get(int rowNum, int colNum){
	if(rowNum < numRows && colNum < numCols){
		return matrix[rowNum]->tuple[colNum];
	}
	return 0;
}

void Matrix :: appendRow(Tuple * appRow){

	if(numCols == appRow->getNumElements()){
		numRows ++;
		matrix.push_back(appRow);
	}
}
