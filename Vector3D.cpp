/*
===============================================================================

	FILE:  Vector3D.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Vector 3D
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "Vector3D.h"
#include <stdio.h>

// Default Constructor
Vector3D :: Vector3D(){
	x = y = z = 0;
}

// Parametized Constructor
Vector3D :: Vector3D(float xVal, float yVal, float zVal){
	x = xVal; y = yVal; z = zVal;
}

// Parametized Constructor
Vector3D :: Vector3D(const Vector3D & v){
	x = v.x ; y = v.y; z = v.z;
}

// Take the cross product and return the vector
Vector3D Vector3D :: cross(const Vector3D & v){
	float c_x = y * v.z - z * v.y;
	float c_y = z * v.x - x * v.z;
	float c_z = x * v.y - y * v.x;
	
	Vector3D crossed(c_x, c_y, c_z);
	
	return crossed;
}

// Take the dot product and return the value
float Vector3D :: dot(Vector3D v){
	float dot_product = x * v.x + y * v.y + z * v.z;
	return dot_product;
}
		
// Set the vector by value
void Vector3D :: set(float xVal, float yVal, float zVal){
	x = xVal; y = yVal; z = zVal;
}

// Set the vector by vector
void Vector3D :: set(const Vector3D & v){
	x = v.x; y = v.y; z = v.z;
}

// Reverse vector and return
void Vector3D :: reverse(){
	x = -1 * x;
	y = -1 * y;
	z = -1 * z;
}

// Return the difference vector of 2 points
void Vector3D :: setDiff(Point & a, Point & b){
	x = a.x - b.x;
	y = a.y - b.y;
	z = a.z - b.z;
}

// Calculate the magnitude of the vector
float Vector3D :: magnitude(){
	return sqrt(pow(x,2) + pow(y,2) + pow(z,2));
}

// Display the values of the vector
void Vector3D :: display(){
	printf("(%f,%f,%f)\n",x,y,z);
}

// Normalize the vector to have magnitude of one
void Vector3D :: normalize(){

	float mag = magnitude();
	x = x / mag;
	y = y / mag;
	z = z / mag;
}

// Scalarize the vector by a specific factor
void Vector3D :: scalarize(float s){

	x *= s;
	y *= s;
	z *= s;	
}

