/*
===============================================================================

	FILE:  ParameterManager.cpp
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Parameter Manager
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh, Naema Nayyar
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh, Naema Nayyar>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#include "ParameterManager.h"
#include "definition.h"
#include "IntracellularMolecule.h"
#include "MembranousMolecule.h"

extern SBMLDocument_t *d;
extern Model_t *m;
extern Species_t **s;
extern Species_t **e;
extern Species_t **subs;
extern string classInfo;
extern int numEnzyme;
extern int numSubstrate;
extern int numMetabolite;
extern multimap<int, string> molEnzyme;
extern multimap<int, string> molSubstrate;
extern int *numMolEnzyme;
extern int *numMolSubstrate;
extern Reaction_t **rxns;
extern int *reactants;
extern int *products;
extern KineticLaw_t **klaw;
extern int initialMet;
extern int finalMet;
  

// State of the Parameter Manager
bool ParameterManager::isInstantiated = false;

// Pointer to the Parameter Manager
ParameterManager * ParameterManager::params;

//////////////////////////////////////////////////////////////////////
// Get an instance of the Parameter Manager
//
ParameterManager * ParameterManager::getParameterManager(){

	// Create new Parameter Manager if not yet exist
	if(!isInstantiated){
	
		// Create Parameter

		ParameterManager * new_param = new ParameterManager();
		
		// Assign Pointer to existing Parameter Manager
		params = new_param;
		
		// Parameter Manage created
		isInstantiated = true;
	}

	// Return pointer to the current Parameter Manager
	return params;
}

//////////////////////////////////////////////////////////////////////
// Get value of the Parameter by the specified KEY
//
int ParameterManager::get(string key){

	// Iterate through the parameter table
	map<string, int >::const_iterator itr = parameterTable.find(key);
			
	// If the KEY is found in the parameter table
	if(itr != parameterTable.end()){
		
		return (itr->second);
	}
	
	// Return NIL if KEY not found in the parameter table
	return 0;
}



//////////////////////////////////////////////////////////////////////
// Add parameter (key, value) pair
//
void ParameterManager::add(string key, int value){
	parameterTable[key] = value;
}



//////////////////////////////////////////////////////////////////////
// Constructor for the Parameter Manager
// Population the parameter table
//
ParameterManager::ParameterManager(){

	// Load default parameter file
	load(INPUT_FILE_PARAMS);
}

// BY SUKWON
void ParameterManager::loadSBML(const char* fName){
    char *checkMoleculeType = NULL;
    
    ofstream inputFile("inputParam.txt", ios::out);

    inputFile << "X_DIM=10" << endl
        << "Y_DIM=10" << endl
        << "Z_DIM=10" << endl;
    
    d = readSBML(fName);
    m = SBMLDocument_getModel(d);
    cout << Model_getAnnotation(m);

    // modified by Sukwon below
    int numSpecies = Model_getNumSpecies(m);
    cout << endl << numSpecies << endl;
    s = new Species_t* [numSpecies];
    e = new Species_t* [numSpecies];
    subs = new Species_t* [numSpecies];
    reactants = new int [numSpecies];
    products = new int [numSpecies];
    klaw = new KineticLaw_t* [numSpecies];
    numMolEnzyme = new int [numSpecies]; 
    numMolSubstrate = new int [numSpecies];
    initialMet = -1;
    finalMet = -1;
    
    int numRxns = Model_getNumReactions(m);
    rxns = new Reaction_t* [numRxns];

    for(int i=0; i<numSpecies; i++) {
        reactants[i] = -1;
        products[i] = -1;
        e[numSpecies] = NULL;
    }
    
    /*rxns = new Reaction_t* [numRxns];
    for(int i=0; i<numRxns; i++)
    {
        rxns[i] = Model_getReaction(m, i);
        int numReactants = Reaction_getNumReactants(rxns[i]);
        int numProducts = Reaction_getNumProducts(rxns[i]);
        int numMods = Reaction_getNumModifiers(rxns[i]);
        const char* tempId;
        for (int j=0; j<numReactants; j++) {
        	tempId = SimpleSpeciesReference_getSpecies(Reaction_getReactant(rxns[i], j));
        	Model_getSpeciesById(m, tempId);
        }
    }*/
    
    for(int i=0; i<numRxns; i++) {
        rxns[i] = Model_getReaction(m, i);
    }
    
    Species_t* tempSp;
    
    string empt = "";
    for(int i=0; i<numSpecies; i++) {
        s[i] = Model_getSpecies(m, i);
        classInfo = SBase_getAnnotation(s[i]);
        checkMoleculeType = strstr (classInfo.c_str(), "PROTEIN");
		cout << "Species #" << i << " is " << Species_getName(s[i]) << endl;
        if(checkMoleculeType != NULL) {
        	cout << "It's a protein!" << endl;
        	for(int j=0; j < numRxns; j++) {
        		cerr << "Reaction #" << j << " out of " << numRxns;
            	for(int mods=0; mods<Reaction_getNumModifiers(rxns[j]); mods++) {
            		cerr << " Checking modifiers.";
		        	if(strcmp(SimpleSpeciesReference_getSpecies(Reaction_getModifier(rxns[j], mods)), Species_getId(s[i])) == 0) {
		        		cerr << " It's a modifier! "; //cerr << e[numEnzyme] << " ~~ "; 		        		
		        		if(false) {
		        			cerr << "Is it ever null?" << endl;
			        		e[numEnzyme] = Model_getSpecies(m, i);
    	        			numMolEnzyme[numEnzyme] = Species_getInitialAmount(s[i]);
		        		} else //if(strcmp(Species_getId(e[numEnzyme]), empt.c_str()) == 0) {
		        		{	cerr << "Is it ever empty?" << endl;
			        		e[numEnzyme] = Model_getSpecies(m, i);
    	        			numMolEnzyme[numEnzyme] = Species_getInitialAmount(s[i]);
		        		}
		        	   	
		        	   	cerr << Species_getName(s[i]) << " / " << Species_getName(e[numEnzyme]) << " modifies ~ ";
		        	   	//reactant
		        		tempSp = Model_getSpeciesById(m, SimpleSpeciesReference_getSpecies(Reaction_getReactant(rxns[j], 0)));
		        		/*int k;
		        		for(k = 0; k < i; k++) {
						    if(strcmp(Species_getName(tempSp), Species_getName(s[k])) == 0)
					    	    break;
		        		}
		        		if(k==i) {
		        			k = addSubstrate(tempSp);
						}*/
						reactants[numEnzyme] = addSubstrate(tempSp);
		        		cerr << "REACTANT: " << reactants[numEnzyme] << "/" << Species_getName(tempSp) << "; ";
		        				        		
		        		//product
		        		tempSp = Model_getSpeciesById(m, SimpleSpeciesReference_getSpecies(Reaction_getProduct(rxns[j], 0)));
		        		/*for(k = 0; k < i; k++) {
						    if(strcmp(Species_getName(tempSp), Species_getName(s[k])) == 0)
					    	    break;
		        		}
		        		if(k==i) {
							k = addSubstrate(tempSp);
						}*/
		        		products[numEnzyme] = addSubstrate(tempSp);
		        		cerr << "PRODUCT: " << products[numEnzyme] << "/" << Species_getName(tempSp) << endl;
		        		
		        		//kinetic law
		        		klaw[numEnzyme] = Reaction_getKineticLaw(rxns[j]);
		        		//cout << "klaw is null: " << (klaw[numEnzyme] == NULL) << endl;
		        	}
            	}
            	cerr << " Say what?" << endl;
		    }
            if((e[numEnzyme] != NULL) && strcmp(Species_getId(e[numEnzyme]), empt.c_str()) != 0) {
            	cerr << "numEnzyme incremented after " << Species_getName(e[numEnzyme]) << endl;
            	numEnzyme++;
            } else {
            	addSubstrate(s[i]);
            }
        } else {
        	//what about phenotypes, RNA, DNA etc??
        	cerr << "Not a protein." << endl;
        	addSubstrate(s[i]);
        }
    }
    cout << numEnzyme << " enzymes and " << numSubstrate << " substrates" << endl; 
    inputFile.close();
    
    for (int rs=0; rs<numSpecies; rs++) {
    	if(rs>=0) {
    		bool isProduct = false;
    		for (int ps=0; ps<numSpecies; ps++) {
    			if(ps>=0 & products[ps] == reactants[rs]) {
    				isProduct = true;
    			}
    		}
    		if(!isProduct) {
    			if(initialMet > -1) {
    				cout << "Uh oh! More than one initial metabolite." << endl;
    			}
    			initialMet = reactants[rs];
    		}
    	}
    }
    
    for (int ps=0; ps<numSpecies; ps++) {
    	if(ps>=0) {
    		bool isReactant = false;
    		for (int rs=0; rs<numSpecies; rs++) {
    			if(rs>=0 & products[ps] == reactants[rs]) {
    				isReactant = true;
    			}
    		}
    		if(!isReactant) {
    			if(finalMet > -1) {
    				cout << "Uh oh! More than one final metabolite." << endl;
    			}
    			finalMet = products[ps];
    		}
    	}
    }
}

//Check if substrate already exists; if not, add it to the list.
int ParameterManager::addSubstrate(Species_t* sp) {
	cerr << "Adding substrate." << endl;
	int place = 0;
	bool exists = false; 
	for(int j=0; j<numSubstrate; j++) {
		if(strcmp(Species_getId(sp), Species_getId(subs[j])) == 0) {
			exists = true;
			place = j;
			break;
		}
	}
	if(!exists) {
    	subs[numSubstrate] = sp; 
        numMolSubstrate[numSubstrate] = Species_getInitialAmount(sp);
        place = numSubstrate;
   	    numSubstrate++;
	}
	if(strstr(((string)SBase_getAnnotation(sp)).c_str(), "SIMPLE_MOLECULE")) {
		numMetabolite++;
	}
	return place;
}

//////////////////////////////////////////////////////////////////////
// Load parameters from data file 'fName'
//
void ParameterManager::load(string fName){
	string key;
	string value;
	string line;
	string delimiter = "=";
	
	// Open Parameter data file
	ifstream inFile (fName.c_str());
	
	if(inFile){

	// Populate the parameter table with data in the file
	while (!inFile.eof())
	{	
		getline(inFile,line,'\n');
		int keyPos = line.find(delimiter);		
		if(keyPos > 0){
			key = line.substr(0,keyPos);
			value = line.substr(keyPos + delimiter.length());
			
			// Add parameter to the paramater table
			parameterTable[key] = atoi(value.c_str());
		}
	}
	
	// Close Parameter data file
	inFile.close();
	}
}

