/*
===============================================================================

	FILE:  SimEngine.h
    
	PROJECT:
	
		Cell++ Cell Simulation Program
	
	CONTENTS:
	
		Simulation Engine
	
	PROGRAMMERS:
	
		Matthew LK Yip, Chris Sanford, Sukwon Oh
	
	COPYRIGHT:
	
        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA
	
	VERSION:
	
		Cell++ 0.93
	
	CHANGES:
	
		Original release (DA)

===============================================================================
*/

#ifndef SIM_ENGINE_H
#define SIM_ENGINE_H

#ifdef GRAPHICS
#include <GL/glut.h>
#include <GL/gl.h>
#include "glow.h"

GLOW_NAMESPACE_USING

#endif /* GRAPHICS */

#include "Molecule.h"
#include "MembranousMolecule.h"
#include "Matrix.h"
#include "GenSimulation.h"
#include "functionLib.h"
#include "ParameterManager.h"
#include "LatticeEnvironment.h"

extern int xDim, yDim, zDim, concType, showEnvType;
extern float resolution;

class SimEngine{

	public:
		SimEngine();
		~SimEngine();
		
		void simulate();
		void display();
		void displayStats();
		void displayGraph();
		
		bool continueSimulation;
		bool showMetaConc;
		bool showMetaEnz;
		bool showEnv;
		int actionType;
		bool showEnvCyt;
		bool showEnv1;
		bool showEnv2;
		bool showEnv3;
		bool showEnv4;
		Simulation * cell;	
	private:
	
		void setDisplayEnvironment();
		void setDrawingColor(int, bool);
		void drawMolecule(Molecule *);
		void displayMetaboliteConcentration();
		void displayEnvironment();
		void displayNumCycles();
		void plotMolecules();
		void drawCellMembrane();
		void constructLattice();
		
		void displayTotalConcentrations();
		
		float *r, *g, *b;
			
//		Simulation * cell;
		ParameterManager * pm;
		LatticeEnvironment * lEnv;
		
#ifdef GRAPHICS
		//Colors and Materials
		GLfloat COL_MOL_ENZYME_A[];
		GLfloat COL_MOL_ENZYME_B[];
		GLfloat COL_MOL_ENZYME_C[];
		GLfloat COL_MOL_ENZYME_D[];
		GLfloat COL_MOL_ENZYME_E[];

		GLfloat LATTICE[];

		GLUquadricObj *qobj;
#endif /* GRAPHICS */		
};

#endif
